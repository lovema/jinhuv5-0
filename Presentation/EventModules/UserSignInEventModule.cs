﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System;
using Tunynet.Events;

namespace Tunynet.Common
{
    /// <summary>
    ///用户签到事件
    /// </summary>
    public class UserSignInEventModule : IEventMoudle
    {
        private IUserSignInDetailRepository userSignInDetailRepository;
        private IUserSignInRepository userSignInRepository;
        private PointService pointService;

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="userSignInDetailRepository"></param>
        /// <param name="userSignInRepository"></param>
        public UserSignInEventModule(IUserSignInDetailRepository userSignInDetailRepository, IUserSignInRepository userSignInRepository, PointService pointService)
        {
            this.userSignInDetailRepository = userSignInDetailRepository;
            this.userSignInRepository = userSignInRepository;
            this.pointService = pointService;
        }

        /// <summary>
        /// 注册事件处理程序
        /// </summary>
        public void RegisterEventHandler()
        {
            EventBus<UserSignInDetail>.Instance().After += new CommonEventHandler<UserSignInDetail, CommonEventArgs>(UserSignInEventModule_After);
        }

        /// <summary>
        /// 用户签到操作事件
        /// *签到明细
        /// *签到统计
        /// *签到积分
        /// *签到金币
        /// *7日奖励
        /// </summary>
        /// <param name="userSignInDetail">签到明细</param>
        /// <param name="eventArgs"></param>
        private void UserSignInEventModule_After(UserSignInDetail userSignInDetail, CommonEventArgs eventArgs)
        {
            if (eventArgs.EventOperationType == EventOperationType.Instance().Create())
            {
                //每日签到积分&金币奖励
                pointService.GenerateByRole(userSignInDetail.UserId, userSignInDetail.UserId, PointItemKeys.Instance().SignIn(), "每日签到");

                //更新明细的金币和积分
                var pointSignIn = pointService.GetPointItem(PointItemKeys.Instance().SignIn());
                var userSignInService = userSignInDetailRepository.Get(userSignInDetail.Id);
                userSignInService.TradePoints = pointSignIn.TradePoints;
                userSignInService.ExperiencePoints = pointSignIn.ExperiencePoints;
                //更新签到统计
                var userSignIn = userSignInRepository.GetByUserId(userSignInDetail.UserId);
                bool isCreate = false;
                if (userSignIn == null)
                {
                    userSignIn = UserSignIn.New();
                    isCreate = true;
                }
                userSignIn.ContinuedSignCount++;
                userSignIn.MonthSignCount++;
                userSignIn.SignCount++;
                userSignIn.LastSignedIn = DateTime.Now;
                userSignIn.ExperiencePointSum += pointSignIn.ExperiencePoints;
                userSignIn.TradePointSum += pointSignIn.TradePoints;
                userSignIn.UserId = userSignInDetail.UserId;
                //是否7天奖励
                if (userSignIn.ContinuedSignCount % 7 == 0)
                {
                    //连续签到积分&金币奖励
                    pointService.GenerateByRole(userSignInDetail.UserId, userSignInDetail.UserId, PointItemKeys.Instance().SignIn7(), "连续7日签到额外奖励");
                    //更新明细的金币和积分
                    var pointSignIn7 = pointService.GetPointItem(PointItemKeys.Instance().SignIn7());

                    userSignInService.TradePoints += pointSignIn7.TradePoints;
                    userSignInService.ExperiencePoints += pointSignIn7.ExperiencePoints;
                    userSignIn.ExperiencePointSum += pointSignIn7.ExperiencePoints;
                    userSignIn.TradePointSum += pointSignIn7.TradePoints;
                }
                if (isCreate)
                    userSignInRepository.Insert(userSignIn);
                else
                    userSignInRepository.Update(userSignIn);
                userSignInDetailRepository.Update(userSignInService);
            }
        }
    }
}