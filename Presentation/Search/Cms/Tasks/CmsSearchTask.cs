﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.CMS;
using Tunynet.Search;
using Tunynet.Tasks;

namespace Tunynet.Common
{
    /// <summary>
    /// 资讯索引任务
    /// </summary>
    public class CmsSearchTask : ITask
    {
        /// <summary>
        /// 任务执行的内容
        /// </summary>
        /// <param name="taskDetail">任务配置状态信息</param>
        public void Execute(TaskDetail taskDetail)
        {
            CmsSearcher cmsSearcher = (CmsSearcher)SearcherFactory.GetSearcher(CmsSearcher.CODE);

            cmsSearcher.SearchTask();
        }
    }
}