﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dm.Model.V20151123;
using Aliyun.MNS;
using Aliyun.MNS.Model;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web;
using Tunynet.Caching;
using Tunynet.Settings;
using Tunynet.Utilities;

namespace Tunynet.Common
{
    /// <summary>
    /// 手机验证码业务逻辑
    /// </summary>
    public class ValidateCodeService
    {
        private ISettingsManager<SiteSettings> siteSettings;
        private ICacheService cacheService;

        public ValidateCodeService(ISettingsManager<SiteSettings> siteSettings, ICacheService cacheService)
        {
            this.siteSettings = siteSettings;
            this.cacheService = cacheService;
        }

        /// <summary>
        /// 发送验证码
        /// </summary>
        /// <param name="phoneNum">手机号</param>
        /// <param name="expiredMinutes">到期时间限制</param>
        /// <returns></returns>
        public bool Send(string phoneNum, string templateCode, int expiredMinutes = 3)
        {
            string time;
            //给发送短信加了个时间限制 1分钟之内多发 都会失败
            cacheService.TryGetValue(phoneNum + "_Time", out time);
            if (!string.IsNullOrEmpty(time))
            {
                return false;
            }
            int count;
            //给发送短信加了个每日时间限制 1日之内过量就失败
            cacheService.TryGetValue(phoneNum + "_Day", out count);
            if (count >= Convert.ToInt32(SMSConfig.ShortCreedNumber))
            {
                return false;
            }
            var siteSetting = siteSettings.Get();
            string validateCode = new Random().Next(999999).ToString().PadLeft(6, '0');
            /**
             * Step 1. 初始化Client
             */
            IMNS client = new Aliyun.MNS.MNSClient(SMSConfig.AccessKey, SMSConfig.AccessSecret, SMSConfig.Endpoint);
            /**
             * Step 2. 获取主题引用
             */
            Topic topic = client.GetNativeTopic(SMSConfig.TopicName);
            /**
             * Step 3. 生成SMS消息属性
             */
            MessageAttributes messageAttributes = new MessageAttributes();
            BatchSmsAttributes batchSmsAttributes = new BatchSmsAttributes();
            // 3.1 设置发送短信的签名：SMSSignName
            batchSmsAttributes.FreeSignName = SMSConfig.SignName;
            // 3.2 设置发送短信的模板SMSTemplateCode
            batchSmsAttributes.TemplateCode = templateCode;
            Dictionary<string, string> param = new Dictionary<string, string>();
            // 3.3 （如果短信模板中定义了参数）设置短信模板中的参数，发送短信时，会进行替换
            param.Add("product", siteSetting.SiteName.Length > 8 ? siteSetting.SiteName.Substring(0, 8) : siteSetting.SiteName);
            param.Add("code", validateCode);
            // 3.4 设置短信接收者手机号码
            batchSmsAttributes.AddReceiver(phoneNum, param);
            //batchSmsAttributes.AddReceiver("$YourReceiverPhoneNumber2", param);
            messageAttributes.BatchSmsAttributes = batchSmsAttributes;
            PublishMessageRequest request = new PublishMessageRequest();
            request.MessageAttributes = messageAttributes;
            /**
             * Step 4. 设置SMS消息体（必须）
             *
             * 注：目前暂时不支持消息内容为空，需要指定消息内容，不为空即可。
             */
            request.MessageBody = "smsmessage";
            try
            {
                /**
                 * Step 5. 发布SMS消息
                 */
                PublishMessageResponse resp = topic.PublishMessage(request);
                if (resp.HttpStatusCode == System.Net.HttpStatusCode.Created)
                {
                    //验证码储存到缓存中
                    cacheService.Set(phoneNum + "_VerificationCode", EncryptionUtility.MD5(validateCode), new TimeSpan(0, expiredMinutes, 0));
                    //一分钟间隔 超过一分钟才能进行发送
                    cacheService.Set(phoneNum + "_Time", EncryptionUtility.MD5(validateCode), new TimeSpan(0, 1, 0));
                    count++;
                    //每日上限+1
                    cacheService.Set(phoneNum + "_Day", count, new TimeSpan(24, 0, 0));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 发送邮件
        /// </summary>
        /// <param name="user">被发送用户</param>
        /// <param name="subject">标题</param>
        /// <param name="model">发送的内容</param>
        /// <param name="change">是否完善资料中的绑定邮箱</param>
        /// <param name="newEmailAddress">新邮箱地址</param>
        /// <returns></returns>
        public bool EmailSend(IUser user, string subject, MailMessage model, bool change = false, string newEmailAddress = null)
        {
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", SMSConfig.AccessKey, SMSConfig.AccessSecret);
            IAcsClient client = new DefaultAcsClient(profile);
            SingleSendMailRequest request = new SingleSendMailRequest();
            try
            {
                request.AccountName = SMSConfig.AccountName;
                request.FromAlias = model.From.DisplayName.Length > 13 ? model.From.DisplayName.Substring(0, 13) : model.From.DisplayName;
                request.AddressType = 1;
                request.TagName = SMSConfig.TagName;
                request.ReplyToAddress = true;
                if (change)
                {
                    if (string.IsNullOrEmpty(newEmailAddress))
                    {
                        request.ToAddress = user.UserGuid;
                    }
                    else
                    {
                        request.ToAddress = newEmailAddress;
                    }
                }
                else
                    request.ToAddress = user.AccountEmail;
                int count;
                //给发送邮件加了个每日时间限制 1日之内过量就失败
                cacheService.TryGetValue(request.ToAddress + "_EmailDay", out count);
                if (count >= Convert.ToInt32(SMSConfig.MailArticleNumber))
                {
                    return false;
                }

                request.Subject = subject + "(" + model.From.DisplayName + ")";
                request.HtmlBody = model.Body;

                SingleSendMailResponse httpResponse = client.GetAcsResponse(request);
                if (httpResponse.HttpResponse.Status == 200)
                {
                    count++;
                    //每日上限+1
                    cacheService.Set(request.ToAddress + "_EmailDay", count, new TimeSpan(24, 0, 0));
                    return true;
                }
                return false;
            }
            catch (ServerException e)
            {
            }

            return false;
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="phoneNum"></param>
        /// <param name="validateCode"></param>
        /// <returns></returns>
        public ValidateCodeStatus Check(string userName, string validateCode, bool? isMobile = true)
        {
            string cookieName = userName + "_VerificationCode";
            if (!isMobile.Value)
            {
                cookieName = userName + "_EmailVerificationCode";
            }
            string code = string.Empty;
            //从缓存里取验证码
            cacheService.TryGetValue(cookieName, out code);
            if (string.IsNullOrEmpty(code))
                return ValidateCodeStatus.Failure;

            if (EncryptionUtility.MD5(validateCode) != code)
                return ValidateCodeStatus.WrongInput;
            return ValidateCodeStatus.Passed;
        }

        /// <summary>
        /// 创建验证码
        /// </summary>
        /// <param name="phoneNum"></param>
        /// <param name="validateCode"></param>
        public void Create(HttpContextBase httpContext, long phoneNum, string validateCode, int expiredMinutes = 2)
        {
            HttpCookie cookies = new HttpCookie("VerificationCode");
            cookies.Value = EncryptionUtility.MD5(validateCode);
            cookies.Expires = DateTime.Now.AddMinutes(expiredMinutes);
            httpContext.Response.Cookies.Add(cookies);
        }

        public string GetCodeError(ValidateCodeStatus status)
        {
            switch (status)
            {
                case ValidateCodeStatus.Empty:
                    return "未匹配到号码";

                case ValidateCodeStatus.Overtime:
                    return "验证超时，请重新获取";

                case ValidateCodeStatus.WrongInput:
                    return "验证码输入错误";

                case ValidateCodeStatus.Failure:
                    return "验证码已失效";

                default:
                    return "未知错误";
            }
        }
    }
}