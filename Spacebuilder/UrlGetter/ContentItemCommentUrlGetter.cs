﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
using Tunynet.CMS;
using Tunynet.Common;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 资讯评论url获取
    /// </summary>
    public class ContentItemCommentUrlGetter : ICommentUrlGetter
    {
        private Authorizer authorizer;
        private IUserService userService;

        public ContentItemCommentUrlGetter(Authorizer authorizer, IUserService userService)
        {
            this.authorizer = authorizer;
            this.userService = userService;
        }

        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().ContentItem(); }
        }

        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool IsManager(long userId)
        {
            var user = userService.GetUser(userId);
            var result = authorizer.IsCategoryManager(this.TenantTypeId, user, null);
            return result;
        }

        /// <summary>
        /// 获取被评论对象(部分)
        /// </summary>
        /// <param name="commentedObjectId"></param>
        /// <param name="commentId">跳转到具体某一个评论</param>
        /// <returns></returns>
        public CommentedObject GetCommentedObject(long commentedObjectId, long commentId)
        {
            var contentItem = new ContentItemRepository().Get(commentedObjectId);
            if (contentItem != null)
            {
                CommentedObject commentedObject = new CommentedObject();
                if (contentItem.ContentModel.ModelKey == ContentModelKeys.Instance().Image())
                {
                    commentedObject.DetailUrl = SiteUrls.Instance().CommentList(commentedObjectId, TenantTypeIds.Instance().ContentItem(), commentId);
                }
                else if (contentItem.ContentModel.ModelKey == ContentModelKeys.Instance().Video())
                {
                    commentedObject.DetailUrl = SiteUrls.Instance().CMSVideoDetail(commentedObjectId, commentId);
                }
                else
                {
                    commentedObject.DetailUrl = SiteUrls.Instance().CMSDetail(commentedObjectId, commentId);
                }

                commentedObject.Name = contentItem.Subject;
                commentedObject.Author = contentItem.Author;
                commentedObject.UserId = contentItem.UserId;
                commentedObject.contentModelName = contentItem.ContentModel.ModelName;
                return commentedObject;
            }
            return null;
        }
    }
}