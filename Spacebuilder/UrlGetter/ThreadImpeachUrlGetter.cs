﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Common;
using Tunynet.Post;

namespace Tunynet.Spacebuilder
{
    /// <summary>
    /// 贴子被举报Url获取
    /// </summary>
    public class ThreadImpeachUrlGetter : IImpeachUrlGetter
    {
        public ThreadService threadService;

        public ThreadImpeachUrlGetter(ThreadService threadService)
        {
            this.threadService = threadService;
        }

        /// <summary>
        /// 租户类型Id
        /// </summary>
        public string TenantTypeId
        {
            get { return TenantTypeIds.Instance().Thread(); }
        }

        /// <summary>
        /// 获取被举报的对象实体
        /// </summary>
        /// <param name="reportObjectId">被举报相关对象Id</param>
        /// <returns></returns>
        public ImpeachObject GetImpeachObject(long reportObjectId)
        {
            var thread = threadService.Get(reportObjectId);
            if (thread != null)
            {
                ImpeachObject impeachObject = new ImpeachObject()
                {
                    DetailUrl = SiteUrls.Instance().ThreadDetail(reportObjectId),
                    Name = thread.Subject,
                    UserId = thread.UserId
                };
                return impeachObject;
            }
            return new ImpeachObject();
        }
    }
}