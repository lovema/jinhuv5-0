﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

namespace Tunynet.Common
{
    /// <summary>
    /// 通过用户数据仓储实现查询
    /// </summary>
    public class DefaultUserIdToUserGuIdDictionary : UserIdToUserGuidDictionary
    {
        public IUserRepository userRepository { get; set; }

        /// <summary>
        /// 根据用户Id获取UserGUid
        /// </summary>
        /// <returns>
        /// UserGuid
        /// </returns>
        protected override string GetUserGuidByUserId(long userId)
        {
            User user = userRepository.Get(userId);
            if (user != null)
                return user.UserGuid;
            return null;
        }

        /// <summary>
        /// 根据GUId获取用户Id
        /// </summary>
        /// <returns>
        /// Userid
        /// </returns>
        protected override long GetUserIdByUserGuid(string userGUId)
        {
            User user = userRepository.GetUserByGuid(userGUId);
            if (user != null)
                return user.UserId;
            return 0;
        }
    }
}