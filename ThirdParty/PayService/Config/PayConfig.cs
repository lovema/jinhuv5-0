﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------
using System.Configuration;

namespace Tunynet.PayServer
{
    /// <summary>
    /// AliPay&&WxPay配置文件属性
    /// </summary>
    public static class PayConfig
    {
        #region AliPay配置

        /// <summary>
        /// 阿里应用的AliAPPId
        /// </summary>
        public static string AliAPPId
        {
            get
            {
                return ConfigurationManager.AppSettings["AliAPPId"];
            }
        }

        /// <summary>
        /// 阿里私人应用私钥2048模式
        /// </summary>
        public static string AliPrivateKey
        {
            get
            {
                return ConfigurationManager.AppSettings["AliPrivateKey"];
            }
        }

        /// <summary>
        /// 支付宝公钥
        /// </summary>
        public static string AliPublicKey
        {
            get
            {
                return ConfigurationManager.AppSettings["AliPublicKey"];
            }
        }

        #endregion AliPay配置

        #region WxPay配置

        /// <summary>
        /// 微信APPID
        /// </summary>
        public static string WxAPPID
        {
            get
            {
                return ConfigurationManager.AppSettings["WxAPPId"];
            }
        }

        /// <summary>
        /// 微信MCHID
        /// </summary>
        public static string WxMCHID
        {
            get
            {
                return ConfigurationManager.AppSettings["WxMCHId"];
            }
        }

        /// <summary>
        /// 微信KEY
        /// </summary>
        public static string WxKEY
        {
            get
            {
                return ConfigurationManager.AppSettings["WxKEY"];
            }
        }

        /// <summary>
        /// 微信APPSECRET
        /// </summary>
        public static string WxAPPSECRET
        {
            get
            {
                return ConfigurationManager.AppSettings["WxAPPSECRET"];
            }
        }

        #endregion WxPay配置
    }
}